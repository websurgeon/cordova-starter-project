/*global jake, desc, task, fail, complete */
/*jslint node: true */

(function () {
    'use strict';
    var colors = require('pb.cli.colors');

    task('default', ['lint'], function () {
        console.log('default jake task was run!');
    });

    // TODO: split linting into separate concerns (i.e. 'app' & 'build')
    desc('lint everything');
    task('lint', [], function () {
        var lint, files, options, globals, pass;
        lint = require('pb.linting.js');
        outputTitle('Linting Started ...');

        files = new jake.FileList();
        files.include('app/**/*.js');
        files.include('build/**/*.js');
        files.exclude(['node_modules']);

        options = globalLintOptions();
        globals = {};

        pass = lint.validateFileList(files.toArray(), options, globals);
        if (pass) {
            outputTitle(files.toArray().length + ' files linted. All OK!');
        } else {
            console.log(colors.red);
            fail('Lint errors found!\n' + colors.reset);
        }
    });

    desc('Start Testacular server for testing');
    task('testacular', function () {
        sh('node', ['node_modules/testacular/bin/testacular', 'start', 'build/testClient/testacular.conf.js'], null, 'Could not start Testacular server', function (stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            complete(stdout, stderr);
        });
    }, {async: true});

    desc('Test client code');
    task('testClient', function () {

        var config, output, oldStdout;
        config = {};
        output = '';
        oldStdout = process.stdout.write;
        process.stdout.write = function (data) {
            output += data;
            oldStdout.apply(this, arguments);
        };

        require('testacular/lib/runner').run(config, function (exitCode) {
            process.stdout.write = oldStdout;

            if (exitCode) {
                console.log(colors.red);
                fail('Client tests failed' + colors.white + ' (to start server, run \'jake testacular\')' + colors.reset + '\n' );
            }

            if (output.indexOf('TOTAL: 0 SUCCESS') !== -1) {
                console.log(colors.red);
                fail('Client tests did not run!');
                console.log(colors.reset);
            }

            complete();
        });
    }, {async: true});

    desc('Create projects');
    task('createAndroid', function () {
        sh('node', ['build/projects/create.js', 'build/projects/config.json', 'android'], null, 'Could not create android project', function (stdout, stderr) {
            // console.log(stdout);
            // console.log(stderr);
            complete(stdout, stderr);
        });
    }, {async: true});
    task('createIOS', function () {
        sh('node', ['build/projects/create.js', 'build/projects/config.json', 'ios'], null, 'Could not create android project', function (stdout, stderr) {
            // console.log(stdout);
            // console.log(stderr);
            complete(stdout, stderr);
        });
    }, {async: true});
    task('createAll', function () {
        sh('node', ['build/projects/create.js', 'build/projects/config.json'], null, 'Could not create android project', function (stdout, stderr) {
            // console.log(stdout);
            // console.log(stderr);
            complete(stdout, stderr);
        });
    }, {async: true});

    function sh(command, args, options, errorMessage, callback) {
        var child, stdout, stderr, quiet, showCommand;
        options = options || {};
        quiet = options.quiet || false;
        showCommand = options.showCommand || false;
        if (showCommand) {
            console.log('> ' + command + ' ' + args.join(' '));
        }

        // Not using jake.createExec as it adds extra line-feeds into output as of v0.3.7
        child = require('child_process').spawn(command, args, { stdio: 'pipe' });

        // redirect stdout
        stdout = '';
        child.stdout.setEncoding('utf8');
        child.stdout.on('data', function (chunk) {
            stdout += chunk;
            if (!quiet) {
                process.stdout.write(chunk);
            }
        });

        // redirect stderr
        stderr = '';
        child.stderr.setEncoding('utf8');
        child.stderr.on('data', function (chunk) {
            stderr += chunk;
            if (!quiet) {
                process.stderr.write(chunk);
            }
        });

        // handle process exit
        child.on('exit', function (exitCode) {
            if (exitCode !== 0) {
                fail(errorMessage);
            }
        });
        child.on('close', function () {      // 'close' event can happen after 'exit' event
            callback(stdout, stderr);
        });
    }

    function outputTitle(title, failed) {
        var color = failed ? colors.red : colors.green;
        console.log('');
        console.log(color + title + colors.reset);
        console.log('');
    }

    // TODO: get global lint options from .jshintrc file
    function globalLintOptions() {
        return {
            browser: true,
            devel: true,
            indent: 4,
            bitwise: true,
            curly: false,
            eqeqeq: true,
            forin: true,
            immed: true,
            latedef: false,
            newcap: true,
            noarg: true,
            noempty: true,
            nonew: true,
            regexp: true,
            undef: true,
            strict: true,
            trailing: true
        };
    }

}());
