/*global require */
/*jslint node: true */

'use strict';

var fs = require('fs'),
    rimraf = require('rimraf'),
    colors = require('pb.cli.colors');

var configPath = process.argv[2];
var PLATFORM = process.argv[3] || 'all';

var root = 'build/projects';

function loadConfig(cbFunction) {
    cbFunction = cbFunction || function () {};
    fs.readFile(configPath, 'utf8', function (err, data) {
        var config = {};
        if (!err) {
            config = JSON.parse(data);
        }
        cbFunction(err, config);
    });
}

function getPlatformConfig(fConfig, platform) {
    var pConfig, config = {};
    pConfig = (fConfig.platforms || {})[platform] || {};
    config.phoneGap = pConfig.phoneGap || fConfig.phoneGap;
    config.appName = pConfig.appName || fConfig.appName;
    config.packageName = pConfig.packageName || fConfig.packageName;
    config.projectName = pConfig.projectName || fConfig.projectName;
    return config;
}

function sh(command, args, options, errorMessage, callback) {
    var child, stdout, stderr, quiet, showCommand;
    options = options || {};
    quiet = options.quiet || false;
    showCommand = options.showCommand || false;
    if (showCommand) {
        console.log('> ' + command + ' ' + args.join(' '));
    }

    // Not using jake.createExec as it adds extra line-feeds into output as of v0.3.7
    child = require('child_process').spawn(command, args, { stdio: 'pipe' });

    // redirect stdout
    stdout = '';
    child.stdout.setEncoding('utf8');
    child.stdout.on('data', function (chunk) {
        stdout += chunk;
        if (!quiet) {
            process.stdout.write(chunk);
        }
    });

    // redirect stderr
    stderr = '';
    child.stderr.setEncoding('utf8');
    child.stderr.on('data', function (chunk) {
        stderr += chunk;
        if (!quiet) {
            process.stderr.write(chunk);
        }
    });

    // // handle process exit
    // child.on('exit', function (exitCode) {
    //     if (exitCode !== 0) {
    //         fail(errorMessage);
    //     }
    // });
    child.on('close', function () {      // 'close' event can happen after 'exit' event
        callback(stdout, stderr);
    });
}

function createPlatformProject(platform, config) {
    var pConfig, command, args;
    pConfig = getPlatformConfig(config, platform);

    // run phonegap create script for platform
    if (platform === 'android') {
        if (process.platform === 'win32') {
            command = 'cscript';
            args = [pConfig.phoneGap + '/lib/' + platform + '/bin/create.js', 'projects/' + platform + '/' + pConfig.projectName, pConfig.packageName, pConfig.appName];
        } else {
            command = pConfig.phoneGap + '/lib/' + platform + '/bin/create';
            args = ['projects/' + platform + '/' + pConfig.projectName, pConfig.packageName, pConfig.appName];
        }
    } else if (platform === 'ios') {
        if (process.platform === 'win32') {
            console.log(colors.red + 'Not Created' + colors.reset + ': ' + colors.yellow + 'IOS Project creation is not currently supported on win32 platform'+ colors.reset);
        } else {
            command = pConfig.phoneGap + '/lib/' + platform + '/bin/create';
            args = ['projects/' + platform + '/' + pConfig.projectName, pConfig.packageName, pConfig.appName];
        }
    }/* else {
        // TODO: support other mobile platforms
    }*/

    if (command) {
        if (!fs.existsSync('projects')) {
            fs.mkdirSync('projects');
        }

        // remove project/platform directory
        rimraf.sync('projects/' + platform);

        // create project/platform directory
        fs.mkdirSync('projects/' + platform);

        sh(command, args, { quiet: true }, 'Could not create project for ' + platform, function (stdout, stderr) {
            if (stderr) {
                console.log('Error: ' + stderr);
            } else {
                console.log(colors.green + '    Created' + colors.reset + ': projects/' + platform + '/' + pConfig.projectName);
            }
        });
    }
}

loadConfig(function (err, config) {
    if (!err) {
        if (PLATFORM === 'android' || PLATFORM === 'all') {
            createPlatformProject('android', config);
        }
        if (PLATFORM === 'ios' || PLATFORM === 'all') {
            createPlatformProject('ios', config);
        }
    }
});
