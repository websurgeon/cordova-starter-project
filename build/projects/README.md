
## Creating Platform Specific Projects

### Configuring Project Creation

#### Project creation is controlled by the '`build/projects/config.json`' file.

Specify default configuration for all platforms


    {
        "phoneGap": "phonegap/phonegap-2.5.0"
        "appName": "myApp",
        "packageName": "com.websurgeon.myApp",
        "projectName": "myApp",
    }


Optionally specify configuration for each platform
NB: if a platform configuration value is not set then the default value will be used

    {
        "platforms": {
            "android": {
                "appName": "myAppAndroid",
                "packageName": "com.websurgeon.myAppAndroid",
                "projectName": "myAmazingApp"
            },
            "ios": {
                "appName": "myAppIOS",
                "packageName": "com.websurgeon.myAppIOS",
                "projectName": "myApp"
            }
        }
    }

Platform specific configuration can be used along with defaults
NB: if no defaults are set then platform specific values must be set

    {
        "phoneGap": "phonegap/phonegap-2.5.0"
        "packageName": "com.websurgeon.myApp",
        "projectName": "myApp",
        "platforms": {
            "android": {
                "appName": "myAppAndroid",
                "projectName": "myAmazingApp",
            },
            "ios": {
                "appName": "myAppIOS",
            }
        }
    }


### Running Create Tasks

There is a Jake task to create project for each platform
- createAndroid
- createIOS
- CreateAll

e.g. Android
    $ ./jake.sh createAndroid


e.g. All Platforms
    $ ./jake.sh createAll




